package org.wushyrussia.diary.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;


/**
 * @author Wushyrussia
 * Base class for entities
 * created 2021.04.12
 * */
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime createDate;

    private LocalDateTime updateDate;

    @PrePersist
    private void onCreate(){
        createDate = LocalDateTime.now();
    }

    @PreUpdate
    private void onUpdate(){
        updateDate = LocalDateTime.now();
    }

}
